#!/bin/bash

# to run these tests ensure the following is set in config:
# xmltv_file = '/opt/postprocess/sample.shepherd.output.xmltv'
# debug = False
# noop = False
#
# ..and watch the log/console to see what happens

SLEEPTIME=5

touch /tmp/postprocess/blah.ts

echo "ERROR: NO VIDEO FILE" 
./renamer.py "The Fifth Element" /tmp/postprocess/missing_file.ts
sleep $SLEEPTIME

echo "ERROR: MOVIE NOT IN XML" 
./renamer.py "All Your Bases Are Belong To Us" /tmp/postprocess/blah.ts
sleep $SLEEPTIME

echo "OK"
./renamer.py "Things to Come" /tmp/postprocess/blah.ts
sleep $SLEEPTIME