#!/usr/bin/python3

import xml.etree.ElementTree as ET

import Config

#xmltv_file = '/home/hoolio/.shepherd/output.xmltv'

tree = ET.parse(Config.xmltv_file)
root = tree.getroot()

passs = 0
fail = 0

print("Scanning", Config.xmltv_file,"for date info:")

for programme in root.findall('programme'):
    try:
        title = programme.find('title').text
        year = programme.find('date').text

    except AttributeError:
        fail = fail + 1
        pass

    else:
        passs = passs +1
        #print("title: " + title + " year: " + year)

#print("total: ", fail + passs, " fail: ", fail, " pass:", str(passs))
total = fail + passs
percent_fail = fail/total * 100
print("~",str(round(100 - percent_fail)), "percent of", total, "entries have a year")