#!/usr/bin/python3

import xml.etree.ElementTree as ET
import sys
import os
import argparse
import logging
import subprocess

import Config 

## SETUP LOGGING.  This shit is cool.
#
# https://towardsdatascience.com/8-advanced-python-logging-features-that-you-shouldnt-miss-a68a5ef1b62d
#
# we log by invoking logger.<log_level> where:
#   DEBUG and above; to logfile only
#   ERROR to logfile and stderr
#
# .. how cool is that!?

#_log_format_file = "%(asctime)s - [%(levelname)s] - %(name)s - (%(filename)s).%(funcName)s(%(lineno)d) - %(message)s"
#_log_format_file = "%(asctime)s - [%(levelname)s] - (%(filename)s).%(funcName)s(%(lineno)d) - %(message)s"
#_log_format_file = "%(asctime)s - [%(levelname)s] - %(funcName)s(%(lineno)d) - %(message)s"
_log_format_file = "%(asctime)s - [%(levelname)s] - %(message)s"
_log_format_stdout = "%(levelname)s: %(message)s"

def get_file_handler():
    file_handler = logging.FileHandler(Config.log_file)

    if Config.debug == True:
        file_handler.setLevel(logging.DEBUG)
    else:
        file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(logging.Formatter(_log_format_file))
    return file_handler

def get_stream_handler():
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.WARNING)
    stream_handler.setFormatter(logging.Formatter(_log_format_stdout))
    return stream_handler

def get_logger(name):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(get_file_handler())
    logger.addHandler(get_stream_handler())
    return logger
  
logger = get_logger(__name__)

# setup incoming arg parsing
parser = argparse.ArgumentParser()
parser.add_argument("title")
parser.add_argument("filename")

class Renamer():
    def __init__(self, args):
        logger.info("---renamer.py v1.337---")

        # from config
        self.xmltv_file      = Config.xmltv_file
        self.log_file        = Config.log_file
        self.comskip         = Config.comskip
        self.comskip_ini     = Config.comskip_ini

        self.debug           = Config.debug
        self.noop            = Config.noop
        # can't see this changing, but it's a var anyhow
        self.default_recording_file_extension = Config.default_recording_file_extension

        # as arguments
        self.target_movie      = args.title
        self.original_filename = args.filename
        self.target_movie_clean = self.strip_special_chars(self.target_movie, Config.special_characters)

        # TBA
        self.year = None
        self.new_filename = None

        if self.debug == True:
            logger.debug("debug:               " + str(self.debug))
            logger.debug("noop:                " + str(self.noop))
            logger.debug("target_movie:        " + self.target_movie)
            logger.debug("target_movie_clean:  " + self.target_movie_clean)
            logger.debug("xmltv_file:          " + self.xmltv_file)
            logger.debug("original_filename:   " + self.original_filename)
            logger.debug("comskip:             " + str(self.comskip))
            logger.debug("comskip_ini:         " + self.comskip_ini)

    @staticmethod
    def strip_special_chars(original_string, special_chars):

        clean_string=original_string

        for i in special_chars:

            if i == "&":
                clean_string=clean_string.replace(i,"And")
            else:
                # Replace the special character with an empty string
                clean_string=clean_string.replace(i,"")
        # Print the string after removal of special characters
        
        logger.info("Cleaned '{}' to '{}'".format(original_string, clean_string))

        return clean_string


    def get_date_from_xmltv(self):
        logger.info("Searching for '" + self.target_movie + "' in"  + self.xmltv_file)

        tree = ET.parse(self.xmltv_file)
        root = tree.getroot()

        year = None
        movie_found = False

        # iterate through all movies
        for programme in root.findall('programme'):
            title = programme.find('title').text

            # we found the movie we care about
            if title.lower() == self.target_movie.lower():
                logger.info("Found '" + self.target_movie + "' in " + self.xmltv_file)

                movie_found = True

                try:
                    year = str(programme.find('date').text)
                    assert len(year) == 4

                    logger.info("Year for '" + self.target_movie + "' is " + year)
                    return year   

                except AttributeError as ae:
                    # there may be another entry for the same movie so
                    logger.info("This entry for '" + self.target_movie + "' had no year data :(")
                    continue

            else:
                logger.debug("Ignoring '" + title + "'")

        # exceptions
        if movie_found == True and year == None:
            logger.debug("Can't find year for '" + self.target_movie + "' in " + self.xmltv_file)
            raise SystemExit

        if movie_found == False:
            logger.error("Can't find '" + self.target_movie + "' in " + self.xmltv_file)
            raise SystemExit

    def create_new_filename(self):
        # determine path of new file
        path = (os.path.dirname(self.original_filename))
        # create list of filename parts so we can join them with .
        new_name_as_list = str.split(self.target_movie_clean)
        new_name_as_list.append(self.year)
        new_name_as_list.append(self.default_recording_file_extension)
        # this will delimit the list items by . (somehow..)
        new_filename = "."
        new_filename = new_filename.join(new_name_as_list)

        # add the path back in
        new_filename = os.path.join(path, new_filename)

        logger.debug("new_filename: " + new_filename)

        return new_filename

    def rename_file(self):
        # ensure the file exists
        try:
            assert os.path.isfile(self.original_filename)
        except AssertionError:
            logger.error("Can't rename because " + self.original_filename + " doesn't exist!")
            raise SystemExit

        # try and rename it
        try:
            if self.noop == False:  # don't rename files in noop mode
                os.rename(self.original_filename, self.new_filename)
                logger.info("Renamed " + self.original_filename + " to " + self.new_filename)

            else:
                logger.warning("NOT renaming " + self.original_filename + " to " + self.new_filename + " (because noop)")                    
            
        except Exception as e:
            logger.error("Can't rename " + self.original_filename + " because ..")
            print(e)
            raise SystemExit

    def subprocessit(self, command_to_run):
        try:
            if self.noop == True:
                #logger.warning("NOTE: commands are echoed in noop mode")
                logger.warning("NOT executing: `" + command_to_run + "` (because noop)")

            else:
                logger.info("Executing: " + command_to_run)

                child = subprocess.Popen(command_to_run, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
                (stdout, stderr) = child.communicate()

                # convert from byte literal
                stdout = stdout.decode('utf-8')
                stderr = stderr.decode('utf-8')

                # get rid of rubbish chars and set type
                stdout = str(stdout.rstrip())
                stderr = str(stderr.rstrip())
                returncode = int(child.returncode)

                # fail if we don't have what we think we do
                assert isinstance(returncode, int), "ERROR: returncode %s is not an integer." % returncode
                assert isinstance(stdout, str), "ERROR: stdout %s is not a str." % stdout
                assert isinstance(stderr, str), "ERROR: stderr %s is not a str." % stderr

                logger.debug("stdout:     " + str(stdout))
                logger.debug("returncode: " + str(returncode))

                if returncode > 0:
                    logger.error("stderr:     " + str(stderr))
                    raise SystemExit
                else:
                    return (stdout, stderr, returncode)

        except Exception as exception:
            logger.error(exception + " when executing " + command_to_run)
            raise SystemExit

def main():
    args = parser.parse_args()

    # init renamer class and populate variables etc
    pp = Renamer(args)

    # get the year from xml
    pp.year = pp.get_date_from_xmltv()

    # determine new filename
    pp.new_filename = pp.create_new_filename()
     
    # rename the file
    pp.rename_file()

    # maybe run other postprocess tasks
    # run comskip
    if Config.comskip is not None:
        pp.subprocessit(pp.comskip + " --ini=" + pp.comskip_ini + " " + pp.new_filename)
        #postprocess.subprocessit("md5sum /usr/local/bin/comskip")

if __name__ == "__main__":
   main()