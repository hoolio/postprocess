# What is this for

* I use [Tvheadend](https://tvheadend.org/) to record SBS world movies from DVB-T using a [HDHomerun](https://www.silicondust.com/hdhomerun/)
* Tvheadend writes to a file server which shares files to my [Kodi](https://kodi.tv/) media pc.
* I want Kodi to be able to find the metadata of a movie accurately,
* but Kodi struggles because (at best) tvheadend creates files named `Movie-Name.ts`.
* If I (manually) rename the file to include the year `Movie-Name.1985.ts` then Kodi ingests it ok.
* This is a postprocessing script for Tvheadend to make that happen automatically.

## Extra features
* This also executes commskip against the renamed file 
* This does not transcode because I don't bother with that; disk is cheap and electricity is not.

# How does it work?
1. [Shepherd](https://github.com/ShephedProject/shepherd) runs from cron and produces an xmltv file containing high quality EPG data (at least) for SBS World Movies.
1. `renamer.py` is invoked by tvheadend with title `%t` and full path to recording `%f`
1. `renamer.py` searches the xmltv file for the title and if found
1. it renames the original file to include the year, eg `The-Fifth-Element.ts` to `The-Fifth-Element.1997.ts`
1. so that when this file is ingested by Kodi it will go the the correct metadata etc.
1. Everything is logged (via `import python logging`) to `renamer.log` and ERRORS and WARNINGS also go to stdout.
1. Configuration is handled in `Config.py` including debug and noop modes.

> Note: If you're not in Australia, YMMV with Shepherd, sorry about that.  Also if you haven't used Shepherd before then YMMV on that too, again, yeah.. 

> Matches are "successful" when the (the first instance of a) movie name is found in the guide data.

## Notes around guide data sources.
I use OTA EIT (EPG) data for Tvheadend mainly because it's easy to setup.. *shrug*.  This script however uses Sheperd guide data.  In case you were wondering, it probably doesn't matter which you chose for Tvheadend, but I don't think you should use a different source for `renamer.py` as imho you'll suffer issues with quality.

# How to install
1. This was developed and tested on Debian Stretch using python3 and no external dependencies.
1. Install Tvheadend etc; I have to assume you can do that already.
1. Install Shepherd as per its doco, set it up for your region and have it grab (at least) SBS World Movies
1. Run Shepherd manually, and wait till it finishes and note the path of the resulting xmltv file as $xmltv_file.
1. Git clone this repo somewhere; I use `/opt/postprocess`.
1. cd there, copy `Config.py.template` to `Config.py` to make your own config.  see note.
1. edit `Config.py` and set the `xmltv_file` to equal $xmltv_file
1. Add shepherd to cron so that the file gets updated regularly eg:
  ```
  # m h  dom mon dow   command
  0 5 * * * /home/hoolio/.shepherd/shepherd
  ```
1. Enable comslip by setting the path to the comskip binary, or disable comskip by setting `comskip = None`

> NOTE: Your `Config.py` won't be overwritten if you update your install with a  `git pull`.  But keep an eye on `Config.py.template` as new config options will appear there.

### Comskip ini == BETA FEATURE ==
* I have not in the past needed a comskip ini file for SBS world movies.
* I also believe tuning them is somewhat of a black art.  
* But for whatever reason at this point i am trying them out so that's why they're here.
* You can/must specify which ini to use by setting `comskip_ini` in `Config.py`.
* I am using a comskip ini from http://www.kaashoek.com/comskip/viewforum.php?f=7 for Australia.

## On cron scheduling
There is a race condition if a) guide data is brand new when b) an older recording is renamed.  that is, the movie may no longer be present in the (new) guide data.  I avoid that by simply not scheduling recordings for the hour or so after I update Shepherd.

# Test of install 

## Guide data quality
Ensure you're getting the year in your xmltv data file by running `scan_for_date.py`.  It will just iterate through the guide data and see how many programmes have detail on the date.  If you have other channels than SBS World Movies then your percentage would be lower than mine.

```
$ ./scan_for_date.py 
Scanning /opt/postprocess/shepherd.output.xmltv for date info:
~ 92 percent of 88 entries have a year
```
If that worked, then in theory most of the movies postprocessed by `renamer.py` will be renamed successfully.

## Manual execution 
`renamer.py` requires two positional arguments, title and filename as below.
```
 ./renamer.py 
usage: renamer.py [-h] title filename
renamer.py: error: the following arguments are required: title, filename
```
The title is the name of the movie, eg "Late Bloomers" (note quotes) and the filename is the full path to the recording, eg:
```
$ ./renamer.py "Late Bloomers" ./worldmovies/Late.Bloomers.ts
```
In this case this produced no console output because there were no errors. `renamer.log` however shows:
```
2022-12-12 13:06:13,161 - [INFO] - ---renamer.py v1.107---
2022-12-12 13:06:13,162 - [DEBUG] - debug:             True
2022-12-12 13:06:13,162 - [DEBUG] - noop:              False
2022-12-12 13:06:13,162 - [DEBUG] - target_movie:      Late Bloomers
2022-12-12 13:06:13,163 - [DEBUG] - xmltv_file:        /home/hoolio/.shepherd/output.xmltv
2022-12-12 13:06:13,163 - [DEBUG] - original_filename: ./worldmovies/Late.Bloomers.ts
2022-12-12 13:06:13,163 - [DEBUG] - comskip:           /usr/local/bin/comskip
2022-12-12 13:06:13,163 - [INFO] - Searching for 'Late Bloomers' in/home/hoolio/.shepherd/output.xmltv
2022-12-12 13:06:13,344 - [DEBUG] - Ignoring 'Sibyl'
2022-12-12 13:06:13,344 - [DEBUG] - Ignoring 'The Reluctant Fundamentalist'
2022-12-12 13:06:13,344 - [DEBUG] - Ignoring 'Coming Home'
2022-12-12 13:06:13,345 - [DEBUG] - Ignoring 'The Fifth Element'
2022-12-12 13:06:13,345 - [DEBUG] - Ignoring 'Sun Children'
2022-12-12 13:06:13,345 - [DEBUG] - Ignoring 'Dead Ringers'
2022-12-12 13:06:13,345 - [DEBUG] - Ignoring 'The Impossible'
2022-12-12 13:06:13,345 - [DEBUG] - Ignoring 'Creation'
2022-12-12 13:06:13,346 - [DEBUG] - Ignoring 'The Perfect Candidate'
2022-12-12 13:06:13,346 - [INFO] - Found 'Late Bloomers' in /home/hoolio/.shepherd/output.xmltv
2022-12-12 13:06:13,346 - [INFO] - This entry for 'Late Bloomers' had no year data :(
2022-12-12 13:06:13,346 - [DEBUG] - Ignoring 'The Big Short'
2022-12-12 13:06:13,346 - [DEBUG] - Ignoring 'Whiplash'
2022-12-12 13:06:13,347 - [DEBUG] - Ignoring 'Our Kind of Traitor'
2022-12-12 13:06:13,347 - [DEBUG] - Ignoring 'Peninsula'
2022-12-12 13:06:13,347 - [DEBUG] - Ignoring 'Mon Oncle'
2022-12-12 13:06:13,347 - [INFO] - Found 'Late Bloomers' in /home/hoolio/.shepherd/output.xmltv
2022-12-12 13:06:13,348 - [INFO] - Year for 'Late Bloomers' is 2011
2022-12-12 13:06:13,348 - [DEBUG] - new_filename: ./worldmovies/Late.Bloomers.2011.ts
2022-12-12 13:06:13,349 - [INFO] - renamed ./worldmovies/Late.Bloomers.ts to ./worldmovies/Late.Bloomers.2011.ts
2022-12-12 13:06:13,349 - [INFO] - executing: /usr/local/bin/comskip --ini=/opt/postprocess/comskip.ini ./worldmovies/Late.Bloomers.2011.ts
Setting ini file to /opt/postprocess/comskip.ini as per commandline
Using /opt/postprocess/comskip.ini for initiation values.
Commercials were found.
2022-12-12 13:39:57,473 - [DEBUG] - returncode: 0

```

## Practice makes perfect!
You should be comfortable running this manually against pre-existing movies before proceeding.  

Setting `comskip = None` in `Config.py` will speed up your experiments as comskip takes awhile.

# Integration into tvheadend
> Title (`%t`) and filename (`%f`)  are available as variables in the [tvheaded postprocessing system](https://tvheadend.org/projects/tvheadend/wiki/Digital_Video_Recorder_configuration) so we can populate those automatically and run the script as a post processing task. 

1. In Tvheadend go configuration > recording > digital video recorder profiles
1. Edit your chosen recording profile, mine is called 'worldmovies'
1. Set it up with sensible options for you, storage path, permissions etc.
1. Add the postprocessor as follows `/opt/postprocess/renamer.py "%t" %f`
1. Ensure the `hts` user has permissions to write to the log, or just go with `sudo chmod 777 renamer.log`
1. Record something using that profile and check `renamer.log` to see what happened.
1. Generally just go and fix any permissions errors you might have eg:

> if nothing is showing up in the log, try running `renamer.py` as the `hts` user, via `sudo su hts`
> also have a look at the tvheadend log (which ususally goes to syslog) with `cat /var/log/syslog | grep renamer`

if tvheadend is connected up ok, you should get something like this in syslog:
```
Apr 15 12:21:39 tvheadend tvheadend[200]: spawn: Executing "/opt/postprocess/renamer.py"
```
and if there is stdout it will appear there.  

## Example of a failure
I found that `The-Actresses.ts` had not been renamed, so i had a look at the creation date:
```
hoolio@tvheadend:/opt/postprocess/worldmovies:$ lsd
total 7.2G
-rwxrwxrwx+ 1 hts    video  3.2G Dec 12 21:32 The-Actresses.ts
```
Then had a look in syslog (for me that's `/var/log/syslog`) at about that time.  We can easily see what went wrong.
```
Dec 12 21:32:24 tvheadend tvheadend[30338]: spawn: PermissionError: [Errno 13] Permission denied: '/opt/postprocess/renam
er.log'
Dec 12 21:32:24 tvheadend tvheadend[30338]: spawn:     return open(self.baseFilename, self.mode, encoding=self.encoding,
Dec 12 21:32:24 tvheadend tvheadend[30338]: spawn:   File "/usr/lib/python3.9/logging/__init__.py", line 1171, in _open
Dec 12 21:32:24 tvheadend tvheadend[30338]: spawn:     StreamHandler.__init__(self, self._open())
Dec 12 21:32:24 tvheadend tvheadend[30338]: spawn:   File "/usr/lib/python3.9/logging/__init__.py", line 1142, in __init_
_
Dec 12 21:32:24 tvheadend tvheadend[30338]: spawn:     file_handler = logging.FileHandler(Config.log_file)
Dec 12 21:32:24 tvheadend tvheadend[30338]: spawn:   File "/opt/postprocess/renamer.py", line 29, in get_file_handler
Dec 12 21:32:24 tvheadend tvheadend[30338]: spawn:     logger.addHandler(get_file_handler())
Dec 12 21:32:24 tvheadend tvheadend[30338]: spawn:   File "/opt/postprocess/renamer.py", line 47, in get_logger
Dec 12 21:32:24 tvheadend tvheadend[30338]: spawn:     logger = get_logger(__name__)
Dec 12 21:32:24 tvheadend tvheadend[30338]: spawn:   File "/opt/postprocess/renamer.py", line 51, in <module>
Dec 12 21:32:24 tvheadend tvheadend[30338]: spawn: Traceback (most recent call last):

```

# Notifications (there aren't any)  
> I may write more code to fix that.

# That's it.
Go forth and be merry!  Any enjoy some quality SBS World Movies on your own time.

# Appendix

## TVHEADEND postprocessing strings (would be args to this script)
We only use `%t` and `%f`, but for future reference there are more available.

```
  Support format strings:
     Format    Description                                 Example value
     %f        Full path to recording                      /home/user/Videos/News.mkv
     %b        Basename of recording                       News.mkv
     %c        Channel name                                BBC world
     %C        Who created this recording                  user
     %t        Program title                               News
     %d        Program description                         News and stories...
     %S        Start time stamp of recording, UNIX epoch   1224421200
     %E        Stop time stamp of recording, UNIX epoch    1224426600
     %e        Error message                               Aborted by user
```

## How I got here
I struggled working out how to get the year and tried:
1. Googling each recording and renaming the file manually .. 
1. an pseudo automated solution based on [imdbpy](https://imdbpy.github.io/) which interactively helped me match my recordingto a list of movies off imdb (then renamed automatically).
1. A fully automated solution (and a predecessor of this script) which used [xmltv.net](http://www.xmltv.net/) guide data in `.xmltv` format to provide the imdb id of the title, and which then used `imdbpy` to query imdb.com for the year.  It worked, albiet much more slowly than `renamer.py`.  But the problem was that many shows didn't have the imdb id populated; this is the reason why Shepherd exists, because many guide sources are bad.